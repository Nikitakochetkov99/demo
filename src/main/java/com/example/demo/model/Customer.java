package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private int age;

    @Column(name = "email")
    private String email;

    @Column(name = "passpordId")
    private int pasportId;

    @Column(name = "country")
    private String country;

    @Column(name = "active")
    private boolean active;

    public Customer(String name, int age, String email, int passwordId, String country) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.pasportId = passwordId;
        this.country = country;
        this.active = false;
    }

    public Customer() {
    }

    @Override
    public String toString() {
        return "Customer [id=" + id + ", name=" + name + ", age=" + age + ", email=" + email
                + ", passwordId" + pasportId + ", country=" + country + ", active=" + active + "]";
    }
}
