package com.example.demo.repository;

import com.example.demo.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomeRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByAge(int age);

}
